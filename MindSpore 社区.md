# MindSpore 社区
## （一）社区发展现状
MindSpore下载量已超过 87 万，4000+社区贡献者，5000+企业应用上线其中手机应用日均7亿+调用量，220+主流网络模型实现。在 Gitee 上 MindSpore 社区共有 19 个仓库，总的 Star 数达到 1.5 万+，总的 PR 数已经超过 4 万，主仓库 Gitee 指数为 96 分，领跑同类项目。

MindSpore 社区目前形成了 20 多个特别兴趣组（SIG），在国内 15 座城市、海外 7 个国家(俄罗斯、新加坡等) 成立举办开发者线上、线下活动组织 MindSpore Study Group（MSG）；联合启智社区已举办 10 场校园行，并在深圳、武汉、上海等全国重点城市持续开展师资培训巡讲；在南京、武汉、成都、西安等城市连续举办 MSG 企业行活动，场均触达100+当地企业。

MindSpore 社区持续赞助 Valse 峰会、机器之心 NeurlIPS Workshop 等国内重要学术会议和活动，Linux 基金会开源峰会、LFAI & Data China Day 等大型开源会议，以及互联网+、CCF BDCI 等国内知名大赛。荣获互联网+大赛产业赛道金奖、OSCAR 尖峰开源项目及开源社区、可信开源社区评估等奖项及证书。社区专家在国内外知名会议均有大量议题宣讲。

## （二）治理模式概述

### 治理模式设计的初衷

我们一直坚持用发展的眼光看问题，深度学习框架的发展我们认为经历了浅层框架时代和通用框架时代，Caffe、Theano以及Tensorflow、PyTorch分别是前后两个时代的典型代表，深度学习框架目前正在进入以MindSpore为代表的全场景AI计算框架的新时代。

为了加速这个新时代的发展，MindSpore社区尤其重视开放的治理模式，通过开源协作，以社区的力量共同努力解决这个新时代的许多新问题，从而吸引有大量的业界专家与新一代开发者会深入的参与到这个伟大的变革之中，成为MindSpore生态的主力军。

### 治理模式概述

开源社区的治理框架同代码一样，也是处于持续的演进之中。

MindSpore 社区目前对社区治理的理解由三个部分组成：开放性、可信性、多样性

开放性：MindSpore 目前拥有来自欧洲、亚洲、美洲的 14 名技术专家的技术治理委员会（TSC），由开发者组织的兴趣小组（SIG）和工作组（WG）。社区所有组织的会议都要求公开、透明，会前有预告会后有录屏，这保证了开发者对 MindSpore 社区的开放性的信任，反过来也推动了 MindSpore 社区的快速成长。

可信性：MindSpore 社区积极参与 OSCAR 社区及信通院牵头组织的开源可信工作，分享了社区开发者体验 SIG 的大量优秀实践，成为全国首批获得可信开源社区评估证书中的唯一一个 AI 开源项目，并成为可信开源社区的发起成员之一。

多样性：我们在国内和海外，通过组织面向本地开发者、高校师生、上下游企业的社群 MindSpore Study Group， 将来自不同文化、习俗、族群、专业、行业的 AI 爱好者，都纳入到社区大家庭中。开源社区内生就是国际化的开发者共享、共建、共治的生态圈，因此不存在特定的国际化战略和国内、国外市场不同策略的概念。但是社区非常重视各个地区在文化上、制度上等方面的特异性，因此社区通过发起面向本地开发者的MindSpore Study Group社群，尽可能通过去中心化的自组织行为，繁荣所在地的开发者生态，并合规的开展社区活动。此外还有最具特色的 MSG·Women In Tech 活动，在北京上海深圳等地都组织了当地女性开发者社区，更好的推动社区对多样性的支持，弘扬包容的社区文化。

## （三）社区运营实践
我们的社区运营工作有一整套的方法论和制度保证。

在形而上的层面，提出价值框架理论，以价值主义和框架主义的观点，统领我们的具体工作；在具体实施层面，社区运营工作分为四大层面：运营平台、社区治理、开源合作以及基础设施。

- 社区运作与管理：整体原则——以技术为核心、社区高于代码。MindSpore 的社区运作强调以技术作为绝对的内容中心，社区运作管理团队所提炼的可传播性内容必须基于 MindSpore 主要技术特性，同时社区运作管理团队开发 MindSpore 开发者高阶 API 套件 TinyMS 等 MindSpore 周边开源项目，用技术项目运作技术社区。我们格外强调以模型的思维和产品迭代的思路，打造集品牌营销、内容制作与传播、社群活动、技术培训等服务能力的平台化产品；

- 社区治理：则通过引入开放治理架构、打造可信开源社区、弘扬性多样性包容文化，来推动开放协作创新；

- 开源合作：侧重于面向业界现有主流开源社区及基金会的广泛合作，比如与 CNCF 基金会的 Kubeflow 和 Volcano 项目这样的技术合作，和开放原子基金会合作的开源运营手册这样把社区运营都开源出来的运营合作，以及同Eclipse基金会共建AICE Lab、LFAI&Data基金会共建MLWorkflow&Interop委员会等等；

- 基础设施：包括大量的 CI 机器人、开发者体验机器人、同步机器人、CI 集群、数据化运营面板的构造。MindSpore从初始就秉承打造立足中国的全球化开源社区的理念，与全世界的深度学习开发者一起构筑生态。我们选择码云作为主仓正是立足中国的体现，但同时也通过打造实时同步机器人等社区工程化手段，在GitHub维护可以进行开发和接受贡献的镜像仓库，方便世界其他地方的开发者参与社区